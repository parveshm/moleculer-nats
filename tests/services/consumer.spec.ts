import { ServiceBroker, Errors } from "moleculer";

import { Broker } from "../../src/brokers/service-broker";
import { Consumer } from "../../src/services/consumer";

describe("Consumer", () => {
  const broker = new ServiceBroker();
  const consumer = new Consumer(broker);

  beforeAll(() => consumer.broker.start());
  afterAll(() => consumer.broker.stop());

  test("should reject when context not defined", async () => {
    await expect(
      broker.call("Consumer.connect", { config: null })
    ).rejects.toBeInstanceOf(Errors.ValidationError);
  });
});
