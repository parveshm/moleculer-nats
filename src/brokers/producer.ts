import { Broker } from "./service-broker";
import { Producer } from "../services/producer";

//create service and start
const service = new Producer(Broker);
service.broker.start();