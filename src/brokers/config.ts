import { Broker } from "./service-broker";
import { Config } from "../services/config";

//create service and start
const service = new Config(Broker);
service.broker.start();
