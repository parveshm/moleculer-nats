import { Broker } from "./service-broker";
import { Processor } from "../services/processor";

//create service and start
const service = new Processor(Broker);
service.broker.start();