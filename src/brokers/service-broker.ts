import { ServiceBroker } from "moleculer";

export const Broker = new ServiceBroker({
  circuitBreaker: {
    enabled: false,
    maxFailures: 2
  },
  hotReload: true,
  cacher: "memory",
  logger: console,
  transporter: {
    type: "NATS",
    options: {
      urls: [
        "nats://localhost:14222",
        "nats://localhost:24222",
        "nats://localhost:34222"
      ]
    }
  }
});
