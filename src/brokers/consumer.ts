import { Broker } from "./service-broker";
import { Consumer } from "../services/consumer";

//create service and start
const service = new Consumer(Broker);
service.broker.start();
