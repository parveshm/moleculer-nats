import { RabbitDetails } from "../types";
const QUEUECONSUMER = "queue_consumer";
const QUEUEPRODUCER = "queue_producer";
const RabbitServer = {
    "host": "",
    "port": "",
    "user": "",
    "password": "",
    "exchange": {
        "vhost": "",
        "type": "topic",
        "key" : QUEUECONSUMER,
        "name": ""
    }
}

const ConsumerQueue = {
    "qname": QUEUECONSUMER
}

const ProducerQueue = {
    "qname": QUEUECONSUMER
}

export const ConsumerConfig: RabbitDetails = Object.assign({}, RabbitServer, ConsumerQueue);
export const ProducerConfig: RabbitDetails = Object.assign({}, RabbitServer, ProducerQueue);