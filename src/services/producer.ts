import { Service, ServiceBroker, Context, ServiceSchema } from "moleculer";
import _ from "lodash";
import { Channel, Message } from "amqplib";

import { RabbitService } from "../services/rabbitService";
import { RabbitDetails } from "../types";

export class Producer extends Service {
  rabbitDetails: RabbitDetails | undefined;
  channel: Channel | undefined;
  timer: NodeJS.Timer | undefined;
  constructor(broker: ServiceBroker) {
    super(broker);
    this.parseServiceSchema(<ServiceSchema>this.getSchema());
  }

  getSchema(): any | ServiceSchema {
    const started = this.serviceStarted;
    //actions
    const connectRabbit = this.connectRabbit;

    return {
      name: "Producer",

      actions: {
        connect: {
          handler: connectRabbit,
          params: {
            config: "object"
          }
        }
      },
      started: started
    };
  }

  async connectRabbit(serviceContext: Context) {
    await this.startProducer(<RabbitDetails>serviceContext.params.config);
  }

  async startProducer(rabbitDetails: RabbitDetails) {
    if (this.timer)
      this.timer.unref();

    if (this.channel) {
      //compare rabbit details
      if (_.isEqual(this.rabbitDetails, rabbitDetails)) return;
      //disconnect and connect with new details
      this.channel.close();
      this.channel = undefined;
    }
    //connect to rabbitmq
    const rabbitservice = new RabbitService(rabbitDetails);
    this.channel = await rabbitservice.connect();
    this.rabbitDetails = rabbitDetails;

    this.publish();
  }

  publish() {
    this.timer = setTimeout(() => {
      const message = `Test _${Math.random()
        .toString(36)
        .substr(2, 5)}`;
      if (this.channel && this.rabbitDetails) {
        this.channel.publish(
          this.rabbitDetails.exchange.name,
          this.rabbitDetails.exchange.key,
          Buffer.from(message)
        );
      }
      this.publish();
    }, 1000);
  }

  serviceStarted() {
    //read consumer
    this.broker.waitForServices("Config").then(() => {
      this.broker.call("Config.producerConfig").then(async producerConfig => {
        //connect with rabbit
        await this.startProducer(producerConfig);
      });
    });
  }
}
