import { Service, ServiceBroker, Context, ServiceSchema } from "moleculer";
import _ from "lodash";
import { Channel, Message, Replies } from "amqplib";

import { RabbitService } from "../services/rabbitService";
import { RabbitDetails } from "../types";

export class Consumer extends Service {
  rabbitDetails: RabbitDetails | undefined;
  channel: Channel | undefined;
  consumer: Replies.Consume | undefined;

  constructor(broker: ServiceBroker) {
    super(broker);
    this.parseServiceSchema(<ServiceSchema>this.getSchema());
    this.waitForServices("processor");
  }

  getSchema(): any | ServiceSchema {
    const started = this.serviceStarted;
    //actions
    const connectRabbit = this.connectRabbit;
    const nodeDisconnected = this.nodeDisconnected;
    const nodeConnected = this.nodeConnected;

    return {
      name: "Consumer",

      actions: {
        connect: {
          handler: connectRabbit,
          params: {
            config: "object"
          }
        }
      },
      started: started,
      events: {
        "$node.disconnected": nodeDisconnected,
        "$node.connected": nodeConnected
      }
    };
  }

  async connectRabbit(serviceContext: Context) {
    await this.broker.waitForServices("processor");
    await this.startConsumer(<RabbitDetails>serviceContext.params.config);
  }

  async startConsumer(rabbitDetails: RabbitDetails) {
    if (this.channel) {
      //compare rabbit details
      if (_.isEqual(this.rabbitDetails, rabbitDetails)) return;
      //disconnect and connect with new details
      if (this.consumer) await this.channel.cancel(this.consumer.consumerTag);

      await this.channel.close();
      this.consumer = undefined;
      this.channel = undefined;
    }
    //connect to rabbitmq
    const rabbitservice = new RabbitService(rabbitDetails);
    this.channel = await rabbitservice.connect();
    this.rabbitDetails = rabbitDetails;
    this.consumer = await this.channel.consume(
      rabbitDetails.qname,
      async (msg: Message | null) => {
        if (msg && this.channel) {
          await this.broker.call("processor.process", {
            message: msg.content.toString()
          });
          // if (this.rabbitDetails && msg.fields.routingKey === this.rabbitDetails.exchange.key)
          return this.channel.ack(msg);
        }
      }
    );
  }

  serviceStarted() {
    //read consumer
    this.broker.waitForServices(["Config", "processor"]).then(() => {
      this.broker.call("Config.consumerConfig").then(async consumerConfig => {
        //start consumer
        await this.startConsumer(consumerConfig);
      });
    });
  }

  delay(ms: number) {
    return new Promise((resolve, reject) => {
      setTimeout(() => resolve(), ms);
    });
  }
  disconnect() {
    if (this.channel) {
      //disconnect and set channel undefined
      this.channel.close();
      this.channel = undefined;
      console.log("Consumer Disconnected");
    }
  }
  async nodeDisconnected(payload: any, sender: any, eventName: any) {
    try {
      const availability = await this.broker.call("processor.available");
    } catch (e) {
      this.logger.info("Processor Unavailable, Disconnecting...");
      this.disconnect();
    }
  }
  async nodeConnected(payload: any, sender: any, eventName: any) {
    try {
      const availability = await this.broker.call("processor.available");
      this.logger.info("Processor available, Connecting...");
      if (this.rabbitDetails && !this.channel)
        this.startConsumer(this.rabbitDetails);
    } catch (e) {}
  }
}
