import amqplib, { Channel } from "amqplib";
import { RabbitDetails } from "../types";

export class RabbitService {
    rabbitDetails: RabbitDetails;

    constructor(rabbitDetails: RabbitDetails) {
        this.rabbitDetails = rabbitDetails;
    }

    async connect(): Promise<Channel> {
        let url = `amqp://${this.rabbitDetails.user}:${this.rabbitDetails.password}@${this.rabbitDetails.host}`;
        //add vhost
        if (this.rabbitDetails.exchange.vhost) {
            url = url.concat(this.rabbitDetails.exchange.vhost);
        }
        try {
            const connection = await amqplib.connect(url);
            const channel = await connection.createChannel();
            const exchange = await channel.assertExchange(this.rabbitDetails.exchange.name, this.rabbitDetails.exchange.type);
            const queue = await channel.assertQueue(this.rabbitDetails.qname);
            return channel;
        }
        catch (error) {
            throw error;
        }
    }
}