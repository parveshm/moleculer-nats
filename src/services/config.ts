import { Service, ServiceBroker, Context, ServiceSchema } from "moleculer";

import { ConsumerConfig, ProducerConfig } from "../config/configuration";

export class Config extends Service {
  constructor(broker: ServiceBroker) {
    super(broker);
    this.parseServiceSchema(<ServiceSchema>this.getSchema());
  }

  getSchema(): any | ServiceSchema {
    // const connect = this.serviceConnected;
    // const created = this.serviceCreated;
    // const stopped = this.serviceStopped;
    const started = this.serviceStarted;
    //actions
    const consumer = this.getConsumerConfig;
    const producer = this.getProducerConfig;

    return {
      name: "Config",

      actions: {
        consumerConfig: consumer,
        producerConfig: producer
      },
      started: started
    };
  }

  //Consumer Config
  getConsumerConfig(serviceContext: Context) {
    return Promise.resolve(ConsumerConfig);
  }

  //Producer Config
  getProducerConfig(serviceContext: Context) {
    return Promise.resolve(ProducerConfig);
  }

  serviceConnected() { }
  serviceCreated() { }
  serviceStopped() { }

  serviceStarted() {
    //publish consumer config
    this.broker.waitForServices("Consumer").then(async () => {
      await this.broker.call("Consumer.connect", { config: ConsumerConfig });
    });

    //publish producer config
    this.broker.waitForServices("Producer").then(async () => {
      await this.broker.call("Producer.connect", { config: ProducerConfig });
    });
  }
}
