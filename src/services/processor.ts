import { Service, ServiceBroker, ServiceSchema } from "moleculer"
import { Context } from "moleculer";

export class Processor extends Service {
    constructor(broker: ServiceBroker) {
        super(broker);
        this.parseServiceSchema(<ServiceSchema>this.getSchema());

    }

    getSchema(): any | ServiceSchema {
        const processMessage = this.process;
        return {
            name: "processor",
            actions: {
                process: {
                    handler: processMessage,
                    params: {
                        "message": "string"
                    }
                },
                available: this.available
            }

        }
    }

    available(serviceContext: Context) {
        return Promise.resolve(true);
    }

    process(serviceContext: Context) {
        const message: string = serviceContext.params.message;
        //validation logic goes here
        return Promise.resolve(true);
    }
}