export type RabbitDetails = {
    "host": string,
    "port": string,
    "user": string,
    "password": string,
    "exchange": {
        "vhost": string,
        "type": string,
        "key": string,
        "name": string
    },
    "qname": string
}